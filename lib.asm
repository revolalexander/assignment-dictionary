global exit
global string_length
global parse_uint
global parse_int
global print_newline
global print_uint
global string_equals
global string_copy
global read_char
global print_string
global print_char
global read_word
global read_string
global print_error

%define stdout 1
%define stderr 2
%define buf_size 256

section .text

exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
.loop:
	cmp byte [rdi+rax], 0 
    je .end 
    inc rax
    jnz .loop 
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, stdout
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, stdout
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push rdi
    mov rdi, 0x0A
    call print_char
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 0x0A
    xor r9, r9
.loop:
    xor rdx, rdx
    div r8
    add rdx, '0'
    sub rsp, 1
    mov byte[rsp], dl
    add r9, 1
    cmp rax, 0
    jnz .loop
.print:
    mov rax, 1
    mov rdi, stdout
    mov rdx, r9
    mov rsi, rsp
    syscall
    add rsp, r9
.end:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov r9, rdi
    test rdi, rdi
    jge .print
    mov rdi, '- '
    call print_char
    mov rdi, r9
    neg rdi
.print:
    call print_uint
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
	mov rcx, 0
	push r8
	push r9
.loop:	
	mov r9b, byte[rdi+rcx]
	mov r10b, byte[rsi+rcx]
	cmp r9b,r10b
	jne .notequals
	cmp r9b,0
	je .stop
	inc rcx
	jmp .loop
.stop:
	pop r10
	pop r9
    ret
.notequals:
	mov rax, 0
	pop r10
	pop r9
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    push 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r8
    mov r8, rdi
	push r9
    mov r9, rsi
	push r10
    mov r10, 0
.loop:
	call read_char
	cmp rax, 0     
    je .success      
    cmp rax, 0x20
    je .loop
    cmp rax, 0x9
    je .loop
    cmp rax, 0xA
    je .loop
.loop2:
    cmp r9, r10
    je .fail
    mov [r8 + r10], rax
    inc r10
	call read_char
	cmp rax, 0
	je .success
	cmp rax, 0x20
    je .success
    cmp rax, 0x9
    je .success
    cmp rax, 0xA
    je .success
    jmp .loop2
	
.fail:
	xor rax, rax
	jmp .ret
.success:
	mov qword[r8+r10], 0
	mov rax, r8
	mov rdx, r10
.ret:
	pop r10
	pop r9
	pop r8	
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
    push r8
    mov r8, 0
    mov rdx, 0
    mov rax, 0
.loop:
    mov r8b, [rdi + rdx] 
    cmp r8b, '0'
    jl .exit
    cmp r8b, '9'
	jg .exit 
    sub r8b, '0' 
    imul rax, 10 
    add rax, r8 
    inc rdx 
    jmp .loop
.exit:
    pop r8
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov rdx, 0
    cmp byte[rdi], '- '
    je .minusnum
    jmp parse_uint
.minusnum:
    inc rdi 
    call parse_uint
    cmp rdx, 0
    je .end
    neg rax
    inc rdx
.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    mov r11, 0
.loop:
    cmp byte[rdi+r11], 0
    je .ret        
    cmp r11, rdx
    je .error
    mov al, byte[rdi+r11]
    mov byte[rsi+r11], al
    inc r11
    jmp .loop
.error:
    mov byte[rsi+r11], 0
    mov rax, 0
    ret
.ret:
    mov byte[rsi+r11], 0
    mov rax, r11
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin до тех пор, пока не встречается символ переноса строки
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину cтроки в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_string:
    mov r8, rdi 
    mov r9, rsi 
    mov r10, 0  
.next_char:
    call read_char
    cmp rax, 0xA
    je .success
    inc r10
    cmp r10, r9
    je .error
    mov [r8+r10-1], al
    jmp .next_char
.success:
    mov byte [r8+r10], 0x0
    mov rax, r8
    mov rdx, r10
    ret
.error:
    mov rax, 0
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    call string_length
    mov rsi, rdi    
    mov rdx, rax    
    mov rax, 1      
    mov rdi, stderr      
    syscall
    mov rax, 0
    ret
