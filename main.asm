%include "words.inc"

%define stdout 1
%define stderr 2
%define buf_size 256

global _start

extern print_string
extern find_word
extern read_char
extern print_newline
extern string_length
extern exit

section .data

input_msg: db 'Enter the key: ', 0
key_found_msg: db 'Key value is: ', 0 
key_not_found_msg: db 'This key is not existed', 0xA, 0
key_overflow_msg: db 'Key can not be longer than 256 characters', 10, 0

section .text


_start:
    mov rdi, start_phrase
    call print_string
    push rbp
    mov rbp, rsp
    mov rdi, rsp
    sub rsp, buf_size
    mov rsi, buf_size
    call read_word
    test rax, rax
    jz .key_overflow
    mov rsi, rax
    mov rdi, label
    call find_word
    mov rsp, rbp
    pop rbp
    test rax, rax
    je .key_not_found_msg
    push rax
    mov rdi, key_found_msg
    call print_string
    pop rdi
    call string_length
    inc rdi		 
    add rdi, rax
    call print_string
    call print_newline
    call exit
.skip:
    inc rdi
    cmp byte [rdi], 0
    jne .skip
    inc rdi
    call print_string
    call print_newline
    call exit
.key_not_found:
    mov rdi, msg_key_not_found
    call print_error
    mov rdi, stderr
    call exit
.key_overflow
    mov rdi, key_overflow_msg
    call print_string
    mov rdi, stderr
    call exit
