%macro colon 2
	%ifid %2	
		%ifdef ptr_begin
			%2: dq ptr_begin
		%else
			%2: dq 0
		%endif
		%define ptr_begin %2
	%else
		%fatal "Error in colon: First argument is not a string literal"
	%endif
    %ifstr %1
		db %1, 0
	%else
		%fatal "This key is incorrect"
	%endif
%endmacro
