ASM=nasm
ASMFLAGS=-f elf64
LD=ld

main:	lib.o dict.o main.o
	$(LD) -o $@ $^

lib.o:	lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

dict.o:	dict.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

main.o:	main.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

clean:
	rm *.o main

